<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller{
    public function register(){
        return view('halaman.signup');
    }

    public function welcome(Request $request){
        $nmdepan = $request['fname'];
        $nmbelakang = $request['lname'];

        return view('halaman.utama', ['namadepan' => $nmdepan, 'namabelakang' => $nmbelakang]);
    }
}
